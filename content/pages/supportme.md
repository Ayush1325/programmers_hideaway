+++
title = "Support Me"
path = "supportme"
template = "pages.html"
draft = false
+++

If you like my work, please consider supporting me through one of the following methods:
- [Github Sponsors](https://github.com/sponsors/Ayush1325)
- [LiberaPay](https://liberapay.com/Ayush1325/donate)
- [BuyMeACoffee](https://www.buymeacoffee.com/ayush1325/)
- Bitcoin: `bc1q2ltrmrt7e3qjrasyrpgky5d8p75nzwm6qj7ndx`
- Ethereum: `0x493a09eab5F113B033b17dF7f50339B8487a89A1`
