+++
title = "About"
path = "about"
template = "pages.html"
draft = false
+++

Hello everyone, I am Ayush Singh, a University Student from India. This website is mainly a blog where I write about projects I am working on or things I find interesting.

I mostly do System Programming and am experienced with C, Rust, and Python. I have experience in UEFI, ZerphyrRTOS, and Linux driver development.

I have worked on implementing Rust std support for UEFI (now in nightly) and am working on expanding its capabilities. I am also involved in improving [Beagleconnect Technology](https://docs.beagleboard.org/latest/boards/beagleconnect/index.html) by simplifying its architecture for BeaglePlay + Beagleconnect Freedom.

I am open for Internships/Part-time as a Software Developer. Feel free to check-out my work on this website.

# Interests
- Linux
- Open-Source
- Rust
- Novels
- Anime

# Education
Indian Institute of Technology (ISM), Dhanbad, India<br>
Integrated Master of Technology in Mathematics and Computing<br>
2020 - 2025
